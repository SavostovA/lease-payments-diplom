<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Pricing example · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/pricing/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">

</head>
<body>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Лизинг</h1>
    <p class="lead">Рассчёт периодических лизинговых платежей на основе различных схем.</p>
</div>


<div class="container">
    <div class="card-deck mb-3 text-center">

        <div class="card mb-4 shadow-sm">

            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Регулярные</h4>
            </div>

            <div class="card-text">
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Регулярные лизинговые платежи, производимые через равные интервалы времени в конце или в начале
                        периодов.
                    </li>
                    <br>

                    <div class="card-body">
                        <form method="post" action="Controllers/ActionController.php?action=schema_1">


                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check1" disabled>
                                <label class="form-check-label" for="defaultCheck1">
                                    С учётом удвоеного первого платежа
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check2">
                                <label class="form-check-label" for="defaultCheck2">
                                    С учётом аванса
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check3">
                                <label class="form-check-label" for="defaultCheck3">
                                    С учётом выкупа имущества по остаточной стоимости
                                </label>
                            </div>
                    </div>
                </ul>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-block btn-primary">Рассчитать</button>
                </div>
                </form>
            </div>
        </div>


        <div class="card mb-4 shadow-sm">

            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Регулярные</h4>
            </div>

            <div class="card-text">
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Регулярные лизинговые платежи. Расчёт части погашения долга, выплаты процентов и остатка долга на конец периода.
                    </li>
                    <br>

                    <div class="card-body">
                        <form method="post" action="Controllers/ActionController.php?action=schema_2">

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check1" disabled>
                                <label class="form-check-label" for="defaultCheck1">
                                    С учётом удвоеного первого платежа
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check2" disabled>
                                <label class="form-check-label" for="defaultCheck2">
                                    С учётом аванса
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check3" disabled>
                                <label class="form-check-label" for="defaultCheck3">
                                    С учётом выкупа имущества по остаточной стоимости
                                </label>
                            </div>
                    </div>
                </ul>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-block btn-primary">Рассчитать</button>
                </div>
                </form>
            </div>
        </div>


        <div class="card mb-4 shadow-sm">

            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Нерегулярные</h4>
            </div>

            <div class="card-text">
                <ul class="list-unstyled mt-3 mb-4">
                    <li>Лизинговые платежи, производимые по согласованному с лизингодателем графику, содержащему суммы платежей и их сроки.
                    </li>
                    <br>

                    <div class="card-body">
                        <form method="post" action="Controllers/ActionController.php?action=schema_3">
                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check1" disabled>
                                <label class="form-check-label" for="defaultCheck1">
                                    С учётом удвоеного первого платежа
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check2" disabled>
                                <label class="form-check-label" for="defaultCheck2">
                                    С учётом аванса
                                </label>
                            </div>

                            <div class="form-check" align="left">
                                <input class="form-check-input" type="checkbox" name="check3" disabled>
                                <label class="form-check-label" for="defaultCheck3">
                                    С учётом выкупа имущества по остаточной стоимости
                                </label>
                            </div>
                    </div>
                </ul>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-block btn-primary" disabled>В разработке</button>
                </div>
                </form>
            </div>
        </div>

    </div>
<footer>
    <p class="mt-5 mb-3 text-muted text-center">ver 1.1(beta)</p>
</footer>

</div>
</body>
</html>
