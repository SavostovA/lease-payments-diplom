
<div class="container-fluid">
    <form method="post" action="../Controllers/ActionController.php?action=schema_2">

        <label class="row">
            Стоимость имущества для лизингодателя
            <input type="text" name="property_value" value="<?php if (isset($_POST['property_value'])) echo $_POST['property_value'];?>"
                   class="form-control" autofocus required>
        </label>

        <label class="row">
            Процентная ставка за период %
            <input type="text" name="interest_rate" value="<?php if (isset($_POST['interest_rate'])) echo $_POST['interest_rate'];?>"
                   class="form-control" autofocus required>
        </label>

        <label class="row">
            Срок лизинга
            <input type="text" name="lease_term" value="<?php if (isset($_POST['lease_term'])) echo $_POST['lease_term'];?>"
                   class="form-control" autofocus required>
        </label>

        <?php if (isset($_POST['check1']) && $_POST['check1'] == "on"): ?>

            <input type="hidden" name="check1" value="on">

        <?php endif; ?>

        <?php if (isset($_POST['check2']) && $_POST['check2'] == "on"): ?>

            <input type="hidden" name="check2" value="on">

            <label class="row">
                Выплата аванса
                <input type="text" name="advance_payment" value="<?php if (isset($_POST['advance_payment'])) echo $_POST['advance_payment'];?>"
                       class="form-control" autofocus required>
            </label>
        <?php endif; ?>

        <?php if (isset($_POST['check3']) && $_POST['check3'] == "on"): ?>

            <input type="hidden" name="check3" value="on">

            <label class="row">
                доля в стоимости имущества
                <input type="text" name="residual_value" value="<?php if (isset($_POST['residual_value'])) echo $_POST['residual_value'];?>"
                       class="form-control" autofocus required>
            </label>
        <?php endif; ?>

        <label class="row">
            <button type="submit" class="btn btn-primary btn-lg">Рассчитать</button>
        </label>
    </form>

</div>

<?php
