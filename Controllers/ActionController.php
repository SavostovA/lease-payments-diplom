<?php
require_once("../Models/Schema_1_Models.php");
require_once("../Models/Schema_2_Models.php");

if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "";

//______________________________________________________________________________________________________________________
//TODO: Доделать check1; check 1 + 2; check 1 + 3; check 1 + 2 + 3. Упорядочить все блоки
if ($action == "schema_1") {
    include("../Views/Schema1/Schema_1_Page.php");

    include("../Views/Schema1/Schema_1_Input.php");


    if (!isset($_POST['check1']) && !isset($_POST['check2']) && !isset($_POST['check3']) && isset($_POST['property_value'])) {
        $fixed_payment_amount_post = postnumerando_lease_payments($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);

        $fixed_payment_amount_pre = prenumerando_lease_payments($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);

        include("../Views/Schema1/Schema_1_Result.php");
    } else

        if (isset($_POST['check1']) && !isset($_POST['check2']) && !isset($_POST['check3']) && isset($_POST['property_value'])) {

            $fixed_payment_amount_post = Double_First_Payment_postnumerando($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
            $fixed_payment_amount_pre = Double_First_Payment_prenumerando($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);

            include("../Views/Schema1/Schema_1_Result.php");
        } else

            if (isset($_POST['check2']) && !isset($_POST['check1']) && !isset($_POST['check3']) && isset($_POST['property_value'])) {

                $fixed_payment_amount_post = with_advance_payment_postnumerando($_POST['advance_payment'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
                $fixed_payment_amount_pre = with_advance_payment_prenumerando($_POST['advance_payment'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);

                include("../Views/Schema1/Schema_1_Result.php");
            } else

                if (isset($_POST['check3']) && !isset($_POST['check1']) && !isset($_POST['check2']) && isset($_POST['property_value'])) {

                    $fixed_payment_amount_post = with_residual_value_postnumerando($_POST['residual_value'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
                    $fixed_payment_amount_pre = with_residual_value_prenumerando($_POST['residual_value'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);

                    include("../Views/Schema1/Schema_1_Result.php");
                }


    if (!isset($_POST['check1']) && isset($_POST['check2']) && isset($_POST['check3']) && isset($_POST['property_value'])) {

        $fixed_payment_amount_post = with_residual_value_and_advance_payment_postnumerando($_POST['advance_payment'], $_POST['residual_value'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
        $fixed_payment_amount_pre = with_residual_value_and_advance_payment_prenumerando($_POST['advance_payment'], $_POST['residual_value'], $_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
        include("../Views/Schema1/Schema_1_Result.php");
    }

} else
    //__________________________________________________________________________________________________________________

    if ($action == "schema_2") {
        include("../Views/Schema2/Schema_2_Page.php");

        include("../Views/Schema2/Schema_2_Input.php");

        if (!isset($_POST['check1']) && !isset($_POST['check2']) && !isset($_POST['check3']) && isset($_POST['property_value']))
        {
            $fixed_payment_postnumerando = postnumerando_lease_payments($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
            $non_permanent_payments_postnumerando = non_permanent_payments_postnumerando($_POST["property_value"], $_POST["interest_rate"] / 100, $_POST["lease_term"]);
            $bo = balance_owed($_POST["property_value"], $non_permanent_payments_postnumerando, $_POST["lease_term"]);
            $p = perc($bo, $_POST["interest_rate"] / 100, $_POST["lease_term"]);
            $n =  $_POST["lease_term"];

            include("../Views/Schema2/Schema_2_Result.php");
        }

    } else

        if ($action == "schema_3") {

        };