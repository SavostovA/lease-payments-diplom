<?php

//Коэффициент рассрочки для постоянных рент постнумерандо
function installment_factor_for_payments_postnumerado($interest_rate, $lease_term) //a1
{
 return $installment_factor = $interest_rate / (1 - (1 + $interest_rate)**(-$lease_term));
}

//коэффициент рассрочки для выплат пренумерандо
function installment_factor_for_payments_prenumerado($interest_rate, $lease_term)//a2
{
 return $installment_factor =  installment_factor_for_payments_postnumerado($interest_rate, $lease_term) *  pow((1 + $interest_rate), -1);
}

//размер ежемесячного платежа постнумерандо
function postnumerando_lease_payments($property_value, $interest_rate, $lease_term)
{
    return $fixed_payment_amount = $property_value * installment_factor_for_payments_postnumerado($interest_rate, $lease_term);
}
//размер ежемесячного платежа пренумерандо
function prenumerando_lease_payments($property_value, $interest_rate, $lease_term)
{
    return $fixed_payment_amount = $property_value * installment_factor_for_payments_prenumerado($interest_rate, $lease_term);
}

//размер ежемесячного платежа постнумерандо, при удвоенном первом платеже
function Double_First_Payment_postnumerando($property_value, $interest_rate, $lease_term)
{


}

//размер ежемесячного платежа пренумерандо, при удвоенном первом платеже
function Double_First_Payment_prenumerando($property_value, $interest_rate, $lease_term)
{

}

//размер ежемесячного платежа постнумерандо, с учётом аванса
function with_advance_payment_postnumerando($advance_payment, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount = ($property_value - $advance_payment) * installment_factor_for_payments_postnumerado($interest_rate, $lease_term);

    return $fixed_payment_amount;

}

//размер ежемесячного платежа пренумерандо, с учётом аванса
function with_advance_payment_prenumerando($advance_payment, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount = ($property_value - $advance_payment) * installment_factor_for_payments_prenumerado($interest_rate, $lease_term);

    return $fixed_payment_amount;

}

function with_residual_value_postnumerando($residual_value, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount = $property_value * (1 - $residual_value *  pow(pow((1 + $interest_rate), -1), $lease_term) ) * installment_factor_for_payments_postnumerado($interest_rate, $lease_term);

    return $fixed_payment_amount;

}

function with_residual_value_prenumerando($residual_value, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount = $property_value * (1 - $residual_value *  pow(pow((1 + $interest_rate), -1), $lease_term) ) * installment_factor_for_payments_prenumerado($interest_rate, $lease_term);

    return $fixed_payment_amount;

}

function with_residual_value_and_advance_payment_postnumerando($advance_payment, $residual_value, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount =
        ($property_value * (1 - $residual_value *  pow(pow((1 + $interest_rate), -1), $lease_term) ) - $advance_payment)
        * installment_factor_for_payments_prenumerado($interest_rate, $lease_term);

    return $fixed_payment_amount;
}

function with_residual_value_and_advance_payment_prenumerando($advance_payment, $residual_value, $property_value, $interest_rate, $lease_term)
{
    $fixed_payment_amount =
        ($property_value * (1 - $residual_value *  pow(pow((1 + $interest_rate), -1), $lease_term) ) - $advance_payment)
        * (installment_factor_for_payments_prenumerado($interest_rate, $lease_term) / (1 + $interest_rate));

    return $fixed_payment_amount;
}