<?php
require_once ("../Models/Schema_1_Models.php");

function non_permanent_payments_postnumerando($property_value, $interest_rate, $lease_term)
{
    $fixed_payment_postnumerando = postnumerando_lease_payments($property_value, $interest_rate, $lease_term);

    for ($i = 1; $i <= $lease_term; $i++)
    {
        $D[0] = $property_value;
        $d[1] = $fixed_payment_postnumerando - $property_value * $interest_rate;

        $D[1] = $D[0] - $d[1];

        if($i > 1)
        {
            $d[$i] = $fixed_payment_postnumerando - $D[$i-1] * $interest_rate;
            $D[$i] = $D[$i-1] - $d[$i];
        }



    }

    return $d;
}

function balance_owed($property_value, $d, $lease_term)
{
    for ($i = 1; $i <= $lease_term; $i++)
    {
        $bo[0] = $property_value;
        $bo[$i] = $bo[$i-1] - $d[$i];
    }
    return $bo;
}

function perc($bo, $interest_rate, $lease_term)
{
    for ($i = 1; $i <= $lease_term; $i++)
    {
        $p[0] = $bo[0] * $interest_rate;
        $p[$i] = $bo[$i] * $interest_rate;
    }
    return $p;
}